import tensorflow as tf
import matplotlib.pyplot as plt
from mnist import get_MNIST_training_set, get_random_training_data

n = 3
number_of_steps = 10000
(x_train, y_train) = get_random_training_data(n,1)

model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(2000, activation=tf.nn.relu),
    tf.keras.layers.Dense(1, activation=None)])
model.compile(optimizer=tf.train.GradientDescentOptimizer(0.25/9),
              loss='mse')

model.fit(x_train, y_train, epochs=number_of_steps, batch_size=n)
#model.evaluate(x_test, y_test)
