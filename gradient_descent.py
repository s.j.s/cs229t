import tensorflow as tf
import numpy as np
import scipy as sc
import matplotlib.pyplot as plt
from mnist import get_MNIST_training_set, get_random_training_data


def train():
    # Randomly initialize weights
    w = np.random.randn(d_in, m)
    a = np.random.randint(low=0, high=2, size=m * d_out).reshape(m, d_out)*2-1

    loss_col = []
    for t in range(number_of_steps):
        # Forward pass: compute predicted y
        h = x.dot(w)
        h_relu = np.maximum(h, 0)  # using ReLU as activate function
        y_pred = h_relu.dot(a) / np.sqrt(m)

        # Compute and print loss
        loss = np.square(y_pred - y).sum()  # loss function
        loss_col.append(loss)
        #print(t, loss)
        if(loss < zero):
            break

        # Backpropagate to compute gradients of w and a with respect to loss
        grad_y_pred = (y_pred - y)
        grad_w2 = h_relu.T.dot(grad_y_pred) / np.sqrt(m)
        grad_h_relu = grad_y_pred.dot(a.T)
        grad_h = grad_h_relu.copy()
        grad_h[h < 0] = 0
        grad_w1 = x.T.dot(grad_h)

        # print(grad_w1)
        #grad_w1 = np.minimum(10, grad_w1)
        #grad_w1 = np.maximum(-10, grad_w1)

        # Update weights
        w -= learning_rate * grad_w1
        #a -= learning_rate * grad_w2
    return loss_col


# n is sample size
# d_in is input dimension
# m is hidden dimension; d_out is output dimension.
n = 3
d_in = 28*28
m = 5
d_out = 1
number_of_steps = 20000
zero = 1e-10

for n2 in range(1,10, 1):
    n=2**n2
    print('n = ', n)
    # Create random input and output data
    #x, y = get_MNIST_training_set(n)
    x, y = get_random_training_data(n, d_in=d_in)

    # print(x.shape)
    # print(y.shape)

    learning_rate = 0.25 / n**2
    k = 100

    for m2 in range(n, n**6, n//2):
        m = m2
        print('m = ', m)

        success = 0
        for i in range(k):
            loss_col = train()
            if(loss_col[-1] <= zero):
                success += 1

        print('probability of getting zero training error = ', success/k)
        if success == k:
            break

        plt.semilogy(loss_col)
        plt.show()
