import tensorflow as tf
import numpy as np
import scipy as sc


def get_random_training_data(n, d_in=28*28):
    x_train = np.random.normal(
        loc=0, scale=1, size=n * d_in).reshape(n, d_in)
    y_train = np.random.normal(loc=0, scale=1, size=n).reshape(n, 1)

    mean = x_train.mean(axis=0).reshape(1, -1)
    variance = x_train.var(axis=0).reshape(1, -1)
    x_train = (x_train-mean)  # /variance

    x_norm = np.linalg.norm(x_train, ord=2, axis=1).reshape(-1, 1)
    x_train = x_train / x_norm
    print('x_train.shape = ', x_train.shape)

    return x_train, y_train


def get_MNIST_training_set(n):
    mnist = tf.keras.datasets.mnist

    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    # x_train = np.random.normal(loc=0, scale=1, size=n * 28 * 28).reshape(n, 28, 28)
    p = np.random.permutation(n)
    x_train = x_train[p, ]
    y_train = y_train[p, ]

    # print('data shape =', x_train.shape)
    x_train = x_train.reshape(-1, 28*28)
    y_train = y_train.reshape(-1, 1)

    mean = x_train.mean(axis=0).reshape(1, -1)
    variance = x_train.var(axis=0).reshape(1, -1)
    x_train = (x_train-mean)  # /variance

    x_norm = np.linalg.norm(x_train, ord=2, axis=1).reshape(-1, 1)
    x_train = x_train / x_norm
    print('x_train.shape = ', x_train.shape)
    return x_train, y_train


n = 1000
x_train, _ = get_MNIST_training_set(n)

# calculate H_{i, j}
x_ij = x_train.dot(x_train.transpose())
k = 1000
e = np.zeros(shape=(n, n))
for i in range(k):
    #print(i)
    w = np.random.normal(size=28*28)
    a = (x_train.dot(w) >= 0).reshape(-1, 1)
    e += (a.dot(a.transpose()) * x_ij)

e = e/k

#print(e)
print('lambda_min = ', sc.linalg.eigh(e, eigvals_only=True, eigvals=(0, 0)))
